<?php


require_once __DIR__ . '/../vendor/autoload.php';

use Marchrius\VigLink\Exceptions\VigLinkException;
use Marchrius\VigLink\Models\VigLink;
use Marchrius\VigLink\Models\VigLinkMetrics;
use Marchrius\VigLink\Models\VigLinkRequestMethod;
use Marchrius\VigLink\Models\VigLinkResponseFormat;

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->overload();

try {
    $viglink = new VigLink(array(
        'api_key' => env('VIGLINK_API_KEY'),
        'secret_key' => env('VIGLINK_SECRET_KEY')
    ));
} catch (VigLinkException $e) {
    echo $e->getMessage() . "\n";
    exit(255);
}

$account = $viglink->accountInfo(true, VigLinkResponseFormat::JSON);

$campaigns = $viglink->campaigns(env('VIGLINK_CAMPAIGN_NAME'), VigLinkResponseFormat::JSON);

$merchants = $viglink->merchantStatus(null, null, null, null, null, 'nike.com');

$monetizedLink = $viglink->monetizedLink('https://www.zalando.it/adidas-performance-t-shirt-con-stampa-collegiate-navy-ad542d12m-k11.html', 'txt', null, uniqid(env('VIGLINK_CAMPAIGN_NAME') . '_'));

$redirectLink = $viglink->redirectLink('https://store.nike.com/it/it_it/pd/calze-da-running-dri-fit-lightweight-no-show-tab/pid-11000145/pgid-12161438', 'txt', null, uniqid('92_'));

$cuidRevenues = $viglink->cuidRevenue(VigLink::CUID_MONTH, new \DateTime);

$productMetadata1 = $viglink->productMetadata($monetizedLink);

$productMetadata2 = $viglink->productMetadata($redirectLink);

$productSearch1 = $viglink->productSearch('shoes');

$productSearch2 = $viglink->productSearch('scarpe shoes', array('it', 'en', 'us'));

$productSearch3 = $viglink->productSearch('scarpe', array('it'), null, null, null, null, null, ',13.00');

$aggregatedMetrics1 = $viglink->aggregatedMetrics(null, array(), null, null, null, false, null);

$aggregatedMetrics2 = $viglink->aggregatedMetrics(null, array(), null, '2017-08-25', null, false, null);

$dailyPerformance1 = $viglink->dailyPerformance(VigLinkMetrics::REVENUE, array(), null, null, '2017-08-25', false, null);

$dailyPerformance2 = $viglink->dailyPerformance(VigLinkMetrics::CLICKS, array(), null, null, '2017-08-25', false, null);

exit(0);