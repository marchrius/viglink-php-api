## Changelog

All notable changes to `marchrius/viglink-php-api` will be documented in this file.

**VigLink 0.1.1 - 2017/09/04**

- `release` _cuidRevenue_ method

**VigLink 0.1.0 - 2017/09/04**

- `added` Support for _env()_ with _phpdotenv_ autoload
- `release` _campaigns_ method
- `release` _monetizedLink_ method
- `release` _redirectLink_ method
- `fix` Minor bugs

**VigLink 0.0.1 - 2017/09/04**

- `release` Initial version