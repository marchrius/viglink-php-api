<?php
/**
 * Created by PhpStorm.
 * User: luna
 * Date: 05/09/17
 * Time: 10:27
 */

use Marchrius\VigLink\Exceptions\VigLinkException;

if (! function_exists('viglink_exception')) {
    /**
     * Generate a VigLinkException with given message
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     *
     * @return VigLinkException
     */
    function viglink_exception($message = "", $code = 0, Throwable $previous = null)
    {
        return new VigLinkException($message, $code, $previous);
    }
}
