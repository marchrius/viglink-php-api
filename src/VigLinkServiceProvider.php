<?php
/**
 * Created by PhpStorm.
 * User: luna
 * Date: 04/09/17
 * Time: 15:40
 */

namespace Marchrius\VigLink;


use Illuminate\Support\ServiceProvider;
use Marchrius\VigLink\Models\VigLink;

class VigLinkServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(array(
            __DIR__ . '/config/config.php' => config_path('viglink.php'),
        ), 'config');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['VigLinkApi'] = $this->app->share(function () {
            return new VigLink(config('viglink'));
        });

        $this->app->alias('VigLinkApi', 'VigLink');
        $this->app->alias('VigLinkApi', 'viglink');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return string[]
     */
    public function provides()
    {
        return array(
            'VigLinkApi'
        );
    }
}
