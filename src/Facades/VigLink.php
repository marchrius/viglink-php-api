<?php

namespace Marchrius\VigLink\Facades;

use Illuminate\Support\Facades\Facade;

class VigLink extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'viglink';
    }
}