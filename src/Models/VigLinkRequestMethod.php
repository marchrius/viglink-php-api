<?php
/**
 * Created by PhpStorm.
 * User: luna
 * Date: 11/09/17
 * Time: 12:03
 */

namespace Marchrius\VigLink\Models;

/**
 * Class VigLinkRequestMethod
 * @package Marchrius\VigLink\Models
 */
class VigLinkRequestMethod
{
    const GET = 'get';
    const POST = 'post';
    const DELETE = 'delete';

    /**
     * @param string $string
     * @param bool $strict
     * @return array
     */
    public static function validate($string, $strict = false)
    {
        $enumClassName = get_called_class();
        $reflect = new \ReflectionClass($enumClassName);
        $constants = $reflect->getConstants();

        if ($strict) {
            return in_array(mb_strtolower($string), array_map('mb_strtolower', $constants));
        }

        return in_array($string, $constants);
    }
}