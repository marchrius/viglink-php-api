<?php

namespace Marchrius\VigLink\Models;

/**
 * VigLink API class
 *
 * API Documentation: https://viglink-developer-center.readme.io/docs
 * Class Documentation: https://bitbuket.org/marchrius/viglink-php-api
 *
 * @author Matteo Gaggiano
 * @since 05.09.2017
 * @copyright Matteo Gaggiano - 2017
 * @version 0.0.1
 * @license BSD http://www.opensource.org/licenses/bsd-license.php
 */
class VigLink
{
    /**
     * The API base URL.
     */
    const API_URL = 'https://api.viglink.com/api';

    /**
     * The VigLink API Key.
     *
     * @var string
     */
    private $_apiKey;

    /**
     * The VigLink OAuth API secret.
     *
     * @var string
     */
    private $_secretKey;

    private $_endpoints = array(
        'monetizedLink' => array(
            'endpoint' => 'http://api.viglink.com/api/click',
            'formats' => array(VigLinkResponseFormat::STRING),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::STRING,
            's_format' => false,
            's_api_key' => 'key',
            's_secret_key' => false
        ),
        'redirectLink' => array(
            'endpoint' => 'http://redirect.viglink.com',
            'formats' => array(VigLinkResponseFormat::RAW_URL),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::RAW_URL,
            's_format' => false,
            's_api_key' => 'key',
            's_secret_key' => false
        ),
        'cuidRevenue' => array(
            'endpoint' => 'https://publishers.viglink.com/service/v1/cuidRevenue',
            'formats' => array(VigLinkResponseFormat::JSON),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::JSON,
            's_format' => false,
            's_api_key' => false,
            's_secret_key' => 'secret'
        ),
        'merchantStatus' => array(
            'endpoint' => 'https://publishers.viglink.com/api/merchant/search',
            'formats' => array(VigLinkResponseFormat::JSON),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::JSON,
            's_format' => false,
            's_api_key' => false,
            's_secret_key' => false
        ),
        'accountInfo' => array(
            'endpoint' => 'https://rest.viglink.com/api/account',
            'formats' => array(VigLinkResponseFormat::JSON, VigLinkResponseFormat::XML),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::JSON,
            's_format' => true,
            's_api_key' => false,
            's_secret_key' => false
        ),
        'campaigns' => array(
            'endpoint' => 'https://rest.viglink.com/api/account/campaigns/:search',
            'formats' => array(VigLinkResponseFormat::JSON, VigLinkResponseFormat::XML),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::JSON,
            's_format' => true,
            's_api_key' => false,
            's_secret_key' => false
        ),
        'productMetadata' => array(
            'endpoint' => 'https://rest.viglink.com/api/product/metadata',
            'formats' => array(VigLinkResponseFormat::JSON, VigLinkResponseFormat::XML),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::JSON,
            's_format' => true,
            's_api_key' => false,
            's_secret_key' => false
        ),
        'productSearch' => array(
            'endpoint' => 'https://rest.viglink.com/api/product/search',
            'formats' => array(VigLinkResponseFormat::JSON, VigLinkResponseFormat::XML, VigLinkResponseFormat::CSV),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::JSON,
            's_format' => true,
            's_api_key' => 'apiKey',
            's_secret_key' => false
        ),
        'aggregatedMetrics' => array(
            'endpoint' => 'https://rest.viglink.com/api/metrics',
            'formats' => array(VigLinkResponseFormat::JSON, VigLinkResponseFormat::XML, VigLinkResponseFormat::CSV),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::JSON,
            's_format' => true,
            's_api_key' => false,
            's_secret_key' => false
        ),
        'dailyPerformance' => array(
            'endpoint' => 'https://rest.viglink.com/api/performance/daily',
            'formats' => array(VigLinkResponseFormat::JSON, VigLinkResponseFormat::XML),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::JSON,
            's_format' => true,
            's_api_key' => false,
            's_secret_key' => false
        ),
        'rankedReports' => array(
            'endpoint' => 'https://rest.viglink.com/api/performance/ranked/:groupBy',
            'formats' => array(VigLinkResponseFormat::JSON, VigLinkResponseFormat::XML, VigLinkResponseFormat::CSV),
            'method' => VigLinkRequestMethod::GET,
            'd_format' => VigLinkResponseFormat::JSON,
            's_format' => true,
            's_api_key' => false,
            's_secret_key' => false
        )
    );

    /**
     * Available cuid period
     */
    const CUID_DAY = 'day';
    const CUID_WEEK = 'week';
    const CUID_MONTH = 'month';

    /**
     * Default constructor.
     *
     * @param array|string $config VigLink configuration data
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     */
    public function __construct($config = null)
    {
        if (is_null($config)) {
            // if you want to access user data
            $this->setApiKey(config('viglink.api_key'));
            $this->setSecretKey(config('viglink.secret_key'));
        } elseif (is_array($config) &&
            isset($config['api_key'], $config['secret_key']) &&
            is_string($config['api_key']) &&
            is_string($config['secret_key'])
        ) {
            // if you want to access user data
            $this->setApiKey($config['api_key']);
            $this->setSecretKey($config['secret_key']);
        } else {
            throw viglink_exception('Error: __construct() - Configuration data is missing.');
        }
    }

    /**
     * @param $function
     *
     * @return null|string
     */
    private function _defaultFormat($function)
    {
        if (is_string($function)) {
            return $this->_endpoints[$function]['d_format'];
        }
        return null;
    }

    /**
     * @param $function
     * @param $format
     *
     * @return null|string
     */
    private function _checkFormat($function, $format)
    {
        $method_formats = $this->_endpoints[$function]['formats'];

        if (!is_string($format) || !in_array($format, $method_formats)) {
            return $this->_defaultFormat($function);
        }

        return $format;
    }

    /**
     * Get your account information and campaign details.
     *
     * @param bool $includeCampaigns By default, a list of your campaigns is returned. To return just your account data, send false
     * @param string $format Lowercase format of response: json or xml
     *
     * @return \stdClass|\SimpleXMLElement
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     */
    public function accountInfo($includeCampaigns = true, $format = null)
    {
        $params = array(
            'includeCampaigns' => !!$includeCampaigns
        );

        $response = $this->_makeCall(__FUNCTION__, $params, null, $format);

        if ($includeCampaigns) {
            return $response;
        }

        return $response->account;
    }

    /**
     * Get a list of campaigns, or search for a specific campaign by name or campaignId.
     *
     * @param string $search Search by campaign name or campaignId
     * @param string $format Lowercase format of response: json or xml
     *
     * @return \stdClass|\SimpleXMLElement
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     */
    public function campaigns($search = 'PRIMARY', $format = null)
    {
        $params = array();

        if (!is_string($search) && !is_numeric($search)) {
            $search = 'PRIMARY';
        }

        $response = $this->_makeCall(__FUNCTION__, $params, null, $format, array(
            'search' => $search
        ));

        return $response->campaigns;
    }

    /**
     * The VigLink Merchant API let’s you determine approval status and access estimated commission rates at scale.
     *
     * @param string|null $keyword Searches for a keyword in the merchant name. Leave empty for a full list of merchants
     * @param string|null $category Searches for merchants within a specific category
     * @param \DateTime|integer|null $update_date Searches for merchants updated on a specific date
     * @param \DateTime|integer|null $create_date Searches for merchants created on a specific date
     * @param boolean|null $insider Searches for merchants part of the Viglink insider program with increased commission rates
     * @param string|null $domain Searches for merchants that can affiliate a specific domain. Leave empty for a full list of merchants
     * @param integer $page Specifies a page of the results to return. Defaults to 100 results per page
     * @param string $format Response format
     *
     * @return \stdClass|\SimpleXMLElement
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     */
    public function merchantStatus(
        $keyword = null,
        $category = null,
        $update_date = null,
        $create_date = null,
        $insider = null,
        $domain = null,
        $page = 1,
        $format = null)
    {
        $date_format = 'Y-m-d';

        $date = new \DateTime;

        $create_date = is_numeric($create_date) ? $date->setTimestamp(intval($create_date)) : is_a($create_date, 'DateTime') ? $create_date : null;
        $update_date = is_numeric($update_date) ? $date->setTimestamp(intval($update_date)) : is_a($update_date, 'DateTime') ? $update_date : null;

        $keyword = is_string($keyword) ? $keyword : null;
        $category = is_string($category) ? $category : null;
        $insider = is_bool($insider) ? !!$insider : null;

        $domain = is_string($domain) ? $domain : null;

        $page = is_numeric($page) ? intval($page) : 1;

        $page = $page < 1 ? 1 : $page;

        $params = array(
            'page' => $page
        );

        if (!is_null($create_date)) {
            $params['createDate'] = is_string($create_date) ? $create_date : date_format($create_date, $date_format);
        }

        if (!is_null($update_date)) {
            $params['updateDate'] = is_string($update_date) ? $update_date : date_format($update_date, $date_format);
        }

        if (!is_null($keyword)) {
            $params['keyword'] = $keyword;
        }

        if (!is_null($category)) {
            $params['category'] = $category;
        }

        if (!is_null($domain)) {
            $params['domain'] = $domain;
        }

        if (!is_null($insider)) {
            $params['insider'] = $insider;
        }

        $response = $this->_makeCall(__FUNCTION__, $params, null, $format);

        return $response->merchants;
    }

    /**
     * The VigLink Link Monetization API allows you to monetize links directly with our simple HTTP API.
     *
     * @param string $url The URL to monetize
     * @param string $format This must be set to value of "txt" *Required
     * @param string $loc The URL to the document containing the link being monetized
     * @param string $cuid An ID of your choosing that identifies the clicker, the page the link is on, a campaign or the click itself. See our CUID documentation for details. **32 alphanumeric character limit.
     * @param boolean $reaf By default, VigLink will not modify URLs which are already affiliated. To force re-affiliation for a single URL, set reaf to true. If you’d like to force re-affiliation globally, you can make that change in your VigLink settings.
     * @param string $ref short for “referrer”, ref is the URL of the referring document for the current page. Expanding on the example from loc, if the user came from http://othersite.com to get to http://example.com, ref is http://othersite.com
     * @param string $title The HTML title of the page containing the link to out
     * @param string $txt The HTML link text of the link to out
     *
     * @return \SimpleXMLElement|\stdClass
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     * @throws \Throwable
     */
    public function monetizedLink($url,
                                  $format,
                                  $loc,
                                  $cuid = null,
                                  $reaf = false,
                                  $ref = null,
                                  $title = null,
                                  $txt = null)
    {
        if (!isset($format) || !is_string($format) || empty($format)) {
            $format = 'txt';
        }

        if (!is_string($loc)) {
            $loc = null;
        }

        if (!is_string($url)) {
            $url = null;
        }

        if (is_null($url))
            viglink_exception("out must exists and be a string");

        if ((!is_string($cuid) || strlen($cuid) > 32)) {
            viglink_exception("cuid must me string and less than 32 chars");
        }

        $params = array(
            'format' => $format,
            'loc' => $loc,
            'reaf' => !!$reaf,
            'key' => $this->getApiKey()
        );

        if (!is_string($ref)) {
            $ref = null;
        }

        if (!is_string($title)) {
            $title = null;
        }

        if (!is_string($txt)) {
            $txt = null;
        }

        if (!is_null($loc)) {
            $params['loc'] = $loc;
        }

        if (!is_null($url)) {
            $params['out'] = $url;
        }

        if (!is_null($ref)) {
            $params['ref'] = $ref;
        }

        if (!is_null($title)) {
            $params['title'] = $title;
        }

        if (!is_null($txt)) {
            $params['txt'] = $txt;
        }

        $_link = $this->_makeCall(__FUNCTION__, $params);

        return $_link;
    }

    /**
     * The VigLink Link Monetization API allows you to monetize links directly with our simple HTTP API.
     *
     * @param string $url The URL to monetize
     * @param string $format This must be set to value of "txt" *Required
     * @param string $loc The URL to the document containing the link being monetized
     * @param string $cuid An ID of your choosing that identifies the clicker, the page the link is on, a campaign or the click itself. See our CUID documentation for details. **32 alphanumeric character limit.
     * @param boolean $reaf By default, VigLink will not modify URLs which are already affiliated. To force re-affiliation for a single URL, set reaf to true. If you’d like to force re-affiliation globally, you can make that change in your VigLink settings.
     * @param string $ref short for “referrer”, ref is the URL of the referring document for the current page. Expanding on the example from loc, if the user came from http://othersite.com to get to http://example.com, ref is http://othersite.com
     * @param string $title The HTML title of the page containing the link to out
     * @param string $txt The HTML link text of the link to out
     *
     * @return string
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     * @throws \Throwable
     */
    public function redirectLink($url,
                                 $format,
                                 $loc,
                                 $cuid = null,
                                 $reaf = false,
                                 $ref = null,
                                 $title = null,
                                 $txt = null)
    {

        if (!isset($format) || !is_string($format) || empty($format)) {
            $format = 'txt';
        }

        if (!is_string($loc)) {
            $loc = null;
        }

        if (!is_string($url)) {
            $url = null;
        }

        if (is_null($url)) {
            viglink_exception("u must exists and be a string");
        }

        if ((!is_string($cuid) || strlen($cuid) > 32)) {
            viglink_exception("cuid must me string and less than 32 chars");
        }

        $params = array(
            'format' => $format,
            'loc' => $loc,
            'reaf' => !!$reaf
        );

        if (!is_string($ref)) {
            $ref = null;
        }

        if (!is_string($title)) {
            $title = null;
        }

        if (!is_string($txt)) {
            $txt = null;
        }

        if (!is_null($loc)) {
            $params['loc'] = $loc;
        }

        if (!is_null($url)) {
            $params['u'] = $url;
        }

        if (!is_null($ref)) {
            $params['ref'] = $ref;
        }

        if (!is_null($title)) {
            $params['title'] = $title;
        }

        if (!is_null($txt)) {
            $params['txt'] = $txt;
        }

        return $this->_makeCall(__FUNCTION__, $params);
    }

    /**
     * VigLink's CUID (Customer Unique Identifier) API allows you to identify the clicker;
     * the page the link is on the campaign; or the click itself.
     *
     * @param string $period The time period of the report. Values can be "day", "week", or "month"
     * @param string|integer|\DateTime $lastDate The last date for which data should be returned. lastDate is compared to revenue posting date rather than click or transaction
     *
     * @return \stdClass
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     */
    public function cuidRevenue($period, $lastDate)
    {
        $dateFormat = 'Y/m/d';
        $date = new \DateTime;

        $lastDate = is_string($lastDate) ? $lastDate : is_numeric($lastDate) ? $date->setTimestamp(intval($lastDate)) : is_a($lastDate, 'DateTime') ? $lastDate : null;

        $params = array(
            'period' => $period,
            'lastDate' => is_string($lastDate) ?: date_format($lastDate, $dateFormat)
        );

        $response = $this->_makeCall(__FUNCTION__, $params);

        foreach ($response as $date => &$asDate) {
            if (count($asDate) > 0) {
                $slices = array_splice($asDate, -1, 1);
                foreach ($slices as $slice) {
                    if (!empty($slice)) {
                        $asDate[] = $slice;
                    }
                }
            }
        }

        return $response;
    }

    /**
     * Get back the metadata about an product (Product URL) with this endpoint.
     *
     * @param string $url Product URL to gather metadata for
     * @param string $format Lowercase format of response: json or xml
     *
     * @return \stdClass|\SimpleXMLElement
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     */
    public function productMetadata($url, $format = null)
    {
        $params = array(
            'url' => $url
        );

        return $this->_makeCall(__FUNCTION__, $params, null, $format);
    }

    /**
     * @param string $query Search by product title, merchant, brand, category, upc or a combination of these fields
     * @param array $country Filter results to only include offers from a specific country. Please use ISO Alpha-2 country codes like "us" for United States or "ca" for Canada. By default, products from all countries are returned.
     * @param array $merchant Filter your query by a specific merchant. Currently this filter is case-sensitive. The best way to ensure that it will return accurate results is to filter with a value already discovered.
     * @param array $brand Filter your query by a specific brand. Currently this filter is case-sensitive. The best way to ensure that it will return accurate results is to filter with a value already discovered.
     * @param array $category Filter your query by a specific category. See the full category taxonomy below.
     * @param array $upc Filter your query by a specific UPC code
     * @param array $asin Filter your query by a specific ASIN
     * @param string $price Filter your query by a specific price, or range of prices. Note: Do not use a comma when providing a price above $999.
     * @param string $sortBy By default, the most relevant results will be returned, but you may sort by price high to low, or low to high. Sorting direction for price defaults to ascending and can be changed to descending by using a minus sign (-) prefix: sortBy=-price
     * @param string $facet To group fields together and return their counts, use a facet like: merchants,category for both merchants and root categories. Learn about how facets work below
     * @param int $page Page may not be 0 or a negative number, and may not exceed the total number of items available in a response. If not supplied, the starting page is 1
     * @param int $itemsPerPage Maximum number of items to include in this response. The default is 100
     * @param bool $filterImages Some merchants do not supply an image in their feeds. As such, you have the option here to filter out items that do not have a merchant-supplied image, or correctly structured image URL, by setting filterImages to true
     * @param string $format Lowercase format of response: json, xml or csv
     *
     * @return \SimpleXMLElement|\stdClass
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     */
    public function productSearch($query = null,
                                  $country = array(),
                                  $merchant = array(),
                                  $brand = array(),
                                  $category = array(),
                                  $upc = array(),
                                  $asin = array(),
                                  $price = null,
                                  $sortBy = null,
                                  $facet = null,
                                  $page = 1,
                                  $itemsPerPage = 100,
                                  $filterImages = false,
                                  $format = null)
    {
        $params = array();

        if (!is_null($query) && is_string($query)) {
            $params['query'] = $query;
        }

        if (!empty($country)) {
            $params['country'] = $country;
        }

        if (!empty($merchant)) {
            $params['merchant'] = $merchant;
        }

        if (!empty($brand)) {
            $params['brand'] = $brand;
        }

        if (!empty($category)) {
            $params['category'] = $category;
        }

        if (!empty($upc)) {
            $params['upc'] = $upc;
        }

        if (!empty($asin)) {
            $params['asin'] = $asin;
        }

        if (!is_null($price)) {
            $params['price'] = $price;
        }

        if (!is_null($sortBy)) {
            $params['sortBy'] = $sortBy;
        }

        if (!is_null($facet)) {
            $params['facet'] = $facet;
        }

        $params['page'] = is_null($page) ? 1 : $page > 0 ? $page : 1;

        $params['itemsPerPage'] = is_null($itemsPerPage) ? 100 : $itemsPerPage > 0 ? $itemsPerPage : 100;

        $params['filterImages'] = !!$filterImages;

        return $this->_makeCall(__FUNCTION__, $params, null, $format);
    }

    /**
     * Get the aggregated metrics for your campaigns.
     * @param int $campaignId Numeric ID of your campaign(s)
     * @param array $product Lowercase instance of a product (all, convert, insert or spotlight)
     * @param array $device Lowercase device type (all, unknown, desktop, tablet, mobile)
     * @param int|\DateTime $startDate Start of requested time series
     * @param int|\DateTime $endDate End of requested time series
     * @param bool $compare Automatically compare metrics to adjacent time range and calculate difference in metrics
     * @param string $format Lowercase format of response: json, xml or csv
     *
     * @return \SimpleXMLElement|\stdClass|string
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     * @throws \Throwable
     */
    public function aggregatedMetrics($campaignId = null,
                                      $product = array(),
                                      $device = null,
                                      $startDate = null,
                                      $endDate = null,
                                      $compare = false,
                                      $format = null)
    {
        $dateFormat = 'Y-m-d';
        $date = new \DateTime();

        $params = array();

        if (is_numeric($campaignId)) {
            $params['campaignId'] = $campaignId;
        }

        if (is_array($product) && count($product) > 0) {
            $params['product'] = $product;
        }

        if (is_array($device) && count($device) > 0) {
            $params['device'] = $device;
        }

        $startDate = is_string($startDate) ? $startDate : is_numeric($startDate) ? $date->setTimestamp(intval($startDate)) : is_a($startDate, 'DateTime') ? $startDate : is_string($startDate) ? $startDate : null;
        $endDate = is_string($endDate) ? $endDate : is_numeric($endDate) ? $date->setTimestamp(intval($endDate)) : is_a($endDate, 'DateTime') ? $endDate : null;

        if (!is_null($startDate)) {
            $params['startDate'] = is_string($startDate) ? $startDate : date_format($startDate, $dateFormat);
        }

        if (!is_null($endDate)) {
            $params['endDate'] = is_string($endDate) ? $endDate : date_format($endDate, $dateFormat);
        } elseif (is_null($endDate) && !is_null($startDate)) {
            $_date = new \DateTime();
            $params['endDate'] = date_format($_date, $dateFormat);
        }

        $params['compare'] = !!$compare;

        $response = $this->_makeCall(__FUNCTION__, $params, null, $format);

        if (is_string($response))
            return $response;

        return $response->metrics;
    }

    /**
     * @param string $measure Measure 1 of 6 available metrics: clicks, ctr, epc, pageviews, revenue, sales
     * @param array $campaignId Numeric ID of your campaign(s)
     * @param array $product Lowercase instance of a product (all, convert, insert or spotlight)
     * @param array $device Lowercase device type (all, unknown, desktop, tablet, mobile)
     * @param string|int|\DateTime $startDate Start of requested time series
     * @param string|int|\DateTime $endDate End of requested time series
     * @param string $format Lowercase format of response: json or xml
     *
     * @return \SimpleXMLElement|\stdClass|string
     *
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     */
    public function dailyPerformance($measure = VigLinkMetrics::REVENUE,
                                     $campaignId = array(),
                                     $product = array(),
                                     $device = array(),
                                     $startDate = null,
                                     $endDate = null,
                                     $format = null)
    {
        $dateFormat = 'Y-m-d';
        $date = new \DateTime();

        $params = array();

        if (VigLinkMetrics::validate($measure)) {
            $params['measure'] = $measure;
        }

        if (is_array($campaignId) && count($campaignId) > 0) {
            $params['campaignId'] = $campaignId;
        }

        if (is_array($product) && count($product) > 0) {
            $params['product'] = $product;
        }

        if (is_array($device) && count($device) > 0) {
            $params['device'] = $device;
        }

        $startDate = is_numeric($startDate) ? $date->setTimestamp(intval($startDate)) : is_a($startDate, 'DateTime') ? $startDate : is_string($startDate) ? $startDate : null;
        $endDate = is_numeric($endDate) ? $date->setTimestamp(intval($endDate)) : is_a($endDate, 'DateTime') ? $endDate : is_string($endDate) ? $endDate : null;

        if (!is_null($startDate)) {
            $params['startDate'] = is_string($startDate) ? $startDate : date_format($startDate, $dateFormat);
        }

        if (!is_null($endDate)) {
            $params['endDate'] = is_string($endDate) ? $endDate : date_format($endDate, $dateFormat);
        } elseif (is_null($endDate) && !is_null($startDate)) {
            $params['endDate'] = date_format(new \DateTime(), $dateFormat);
        }

        $response = $this->_makeCall(__FUNCTION__, $params, null, $format);

        return $response;
    }

    /**
     * The call operator.
     *
     * @param string $function API resource path
     * @param array $params Additional request parameters
     * @param string $method Request type GET|POST|DELETE
     * @param string $format Response format SimpleXMLElement or stdClass
     * @param array $replacements Request url placeholder replacers (without double-dots)
     *
     * @return \SimpleXMLElement|\stdClass|string
     * @throws \Marchrius\VigLink\Exceptions\VigLinkException
     */
    protected
    function _makeCall($function, $params = array(), $method = null, $format = null, $replacements = array())
    {
        $paramString = null;

        $settings = $this->_endpoints[$function];

        $supportsFormat = is_bool($settings['s_format']) && !!$settings['s_format'];

        if (is_null($method)) {
            $method = $settings['method'];
        }

        $format = $this->_checkFormat($function, $format);

        if ($supportsFormat) {
            $params['format'] = $format;
        }

        if (is_string($settings['s_api_key'])) {
            $params[$settings['s_api_key']] = $this->getApiKey();
        }

        if (is_string($settings['s_secret_key'])) {
            $params[$settings['s_secret_key']] = $this->getSecretKey();
        }

        if (isset($params) && is_array($params)) {
            $paramString = '?' . http_build_query($params);
        }

        $api_url = $settings['endpoint'] . ((VigLinkRequestMethod::GET === $method) ? $paramString : null);

        if (is_array($replacements) && count($replacements) > 0) {
            foreach ($replacements as $replacer => $replacement) {
                $api_url = preg_replace('/\/\:' . $replacer . '/i', '/' . $replacement, $api_url);
            }
        }

        $headerData = array(
            'Authorization: secret ' . $this->getSecretKey()
        );

        switch ($format) {
            case VigLinkResponseFormat::XML:
                $headerData[] = 'Accept: application/xml';
                break;
            case VigLinkResponseFormat::STRING:
                $headerData[] = 'Accept: text/plain';
                break;
            case VigLinkResponseFormat::RAW_URL:
                return $api_url;
                break;
            case VigLinkResponseFormat::CSV:
                $headerData[] = 'Accept: application/csv';
                break;
            case VigLinkResponseFormat::JSON:
            default:
                $headerData[] = 'Accept: application/json';
                break;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        switch ($method) {
            case VigLinkRequestMethod::POST:
                curl_setopt($ch, CURLOPT_POST, count($params));
                curl_setopt($ch, CURLOPT_POSTFIELDS, ltrim($paramString, '&'));
                break;
            case VigLinkRequestMethod::DELETE:
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, VigLinkRequestMethod::DELETE);
                break;
        }

        $response = curl_exec($ch);

        // split header from JSON data
        // and assign each to a variable
        $response_info = curl_getinfo($ch);

        $response_code = isset($response_info['http_code']) ? intval($response_info['http_code']) : 500;

        $header_size = isset($response_info['header_size']) ? intval($response_info['header_size']) : 0;

        $response_data = substr($response, $header_size);

        switch ($response_code) {
            case 200:
                break;
            case 400:
                throw viglink_exception($response_data);
                break;
            case 401:
                throw viglink_exception('Error: Unauthorized');
                break;
            case 500:
                throw viglink_exception('Error: Internal Server Error');
                break;
            default:
                throw viglink_exception('Error: _makeCall() - cURL error: ' . curl_error($ch));
                break;
        }

        $header_size = isset($response_info['header_size']) ? intval($response_info['header_size']) : 0;

        $response_data = substr($response, $header_size);

        if (strlen($response_data) <= 0) {
            throw viglink_exception('Error: _makeCall() - cURL error: ' . curl_error($ch));
        }

        curl_close($ch);

        switch ($format) {
            case VigLinkResponseFormat::XML:
                return new \SimpleXMLElement($response_data);
            case VigLinkResponseFormat::STRING:
            case VigLinkResponseFormat::CSV:
                return $response_data;
            case VigLinkResponseFormat::JSON:
            default:
                return json_decode($response_data);
        }

    }

    /**
     * API-key Setter
     *
     * @param string $apiKey
     *
     * @return void
     */
    public
    function setApiKey($apiKey)
    {
        $this->_apiKey = $apiKey;
    }

    /**
     * API Key Getter
     *
     * @return string
     */
    public
    function getApiKey()
    {
        return $this->_apiKey;
    }

    /**
     * API Secret Setter
     *
     * @param string $secretKey
     *
     * @return void
     */
    public
    function setSecretKey($secretKey)
    {
        $this->_secretKey = $secretKey;
    }

    /**
     * API Secret Getter.
     *
     * @return string
     */
    public
    function getSecretKey()
    {
        return $this->_secretKey;
    }

}
