<?php
/**
 * Created by PhpStorm.
 * User: luna
 * Date: 11/09/17
 * Time: 12:01
 */

namespace Marchrius\VigLink\Models;

/**
 * Class VigLinkMetrics
 * @package Marchrius\VigLink\Models
 */
class VigLinkMetrics
{
    const SALES = 'sales';
    const REVENUE = 'revenue';
    const CLICKS = 'clicks';
    const CRT = 'ctr';
    const EPC = 'epc';
    const PAGE_VIEWS = 'page_views';

    /**
     * @param string $string
     * @param bool $strict
     * @return array
     */
    public static function validate($string, $strict = false)
    {
        $enumClassName = get_called_class();
        $reflect = new \ReflectionClass($enumClassName);
        $constants = $reflect->getConstants();

        if ($strict) {
            return in_array(mb_strtolower($string), array_map('mb_strtolower', $constants));
        }

        return in_array($string, $constants);
    }
}