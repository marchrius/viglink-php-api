# VigLink PHP API

A PHP wrapper for the VigLink API. Feedback or bug reports are appreciated.

[![Total Downloads](http://img.shields.io/packagist/dt/marchrius/viglink-php-api.svg?style=flat)](https://packagist.org/packages/marchrius/viglink-php-api)
[![Latest Stable Version](http://img.shields.io/packagist/v/marchrius/viglink-php-api.svg?style=flat)](https://packagist.org/packages/marchrius/viglink-php-api)
[![License](https://img.shields.io/packagist/l/marchrius/viglink-php-api.svg?style=flat)](https://packagist.org/packages/marchrius/marchrius/viglink-php-api)

> [Composer](#installation) package available.  

## Requirements

- PHP 5.3 or higher
- cURL
- Registered account on VigLink

## Get started

To use the VigLink API you have to register yourself as a developer at the [VigLink Developer Platform](https://viglink-developer-center.readme.io/docs/overview-1). At [User's campaigns informations](https://publishers.viglink.com/account) you will receive your `api_key` and `secret_key`.

---

> A good place to get started is the [example project](example/README.md).

### Installation

I strongly advice using [Composer](https://getcomposer.org) to keep updates as smooth as possible.

```
$ composer require marchrius/viglink-php-api
```

### Initialize the class

```php
use Marchrius\VigLink\VigLink;

$viglink = new VigLink(array(
	'api_key'      => 'YOUR_APP_KEY',
	'secret_key'   => 'YOUR_APP_SECRET'
));
```

**All methods return the API data `json_decode()` - so you can directly access the data as `stdClass`.**

## Examples

Check out the `examples` directory for standard PHP examples (no laravel integration)
## Changelog

Please see the [changelog file](CHANGELOG.md) for more information.

## Credits

Copyright (c) 2017 - Programmed by Matteo Gaggiano

Released under the [BSD License](LICENSE).
